CREATE DATABASE craiglist;   //Creating database
USE craiglist;		      //Using database



//creating table for regions

CREATE TABLE Regions(
Id INT NOT NULL PRIMARY KEY,
Name VARCHAR(50)
);


//creating table for Categories

CREATE TABLE categories(
categories_Id INT NOT NULL PRIMARY KEY,
name VARCHAR(50)
);

//creating table for users

CREATE TABLE users(
user_Id INT NOT NULL PRIMARY KEY,
name VARCHAR(50));


//creating table for posts

CREATE TABLE posts(
post_Id INT PRIMARY KEY,
title VARCHAR(50),
location VARCHAR(50),
regions_Id INT NOT NULL,
user_Id INT NOT NULL,
categories_Id INT NOT NULL,
FOREIGN KEY (regions_Id) REFERENCES Regions(Id),
FOREIGN KEY (user_Id) REFERENCES users(user_Id),
FOREIGN KEY (categories_Id) REFERENCES categories(categories_Id)
);



//Inserting values to tables

//Inserting values in Regions Table;

INSERT INTO Regions
VALUES (1,'Bangalore'),
	(2,'Hubli'),
	(3,'Mumbai'),
	(4,'Delhi'),
	(5,'Pune'),
	(6,'Kolkatta');
	
	
//Inserting categories
INSERT INTO categories
VALUES (1,'jobs'),
	(2,'housing'),
	(3,'Services'),
	(4,'Delivery');
	
	
//Inserting Users

INSERT INTO users
VALUES (1,'Srikanth'),
	(2,'Jaggdish'),
	(3,'Akshay'),
	(4,'Balaji'),
	(5,'Vishwanath'),
	(6,'Aravind');
	
//Inserting values to post 

INSERT INTO posts
VALUES (1,'software_Developer','Koramangla',1,1,1),
	(2,'software_Developer','koramangla',1,2,1),
	(3,'House_for_sale','Jayanagar',1,3,2),
	(4,'House_for_sale','Basvangudi',1,4,2);

