Queries

1.Number of matches played per year

 SELECT season,count(*) AS no_of_matches 
 FROM matches 
 GROUP BY season
 ORDER BY season;

2.Number of matches Won per team in ipl

SELECT season,winner,COUNT(*) AS number FROM matches
GROUP BY winner,season;

3.Extra runs conceded per team in the year 2016

SELECT bowling_team,SUM(extra_runs) AS extra_runs FROM
matches JOIN deliveries
ON matches.id=deliveries.match_id
WHERE season = 2016
GROUP BY bowling_team
ORDER BY extra_runs 
LIMIT 10;

4.Top 10 economical bowlers in the year 2015;

SELECT bowler,((SUM(total_runs)/(COUNT(total_runs)/6))) 
AS economy FROM matches 
JOIN deliveries
ON matches.id=deliveries.match_id
WHERE season = 2015
GROUP BY bowler
ORDER BY economy 
LIMIT 10;

